/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.util.*;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Vehiculo implements Serializable,Comparable<Vehiculo> {
    private String placa, marca, color, motor,nomFile;
    private int ruedas, año;
    private Venta venta;
    private Descripcion descripcion;
    
    public Vehiculo(){}
    
    public Vehiculo(int año){
        this.año=año;
    }
    
    public Vehiculo(String placa, String marca, String color, String motor, String ruedas, String año,String nomFile) throws Exception{
        if(placa.equals("")||año.equals("")||motor.equals("NO DATA")) throw new InformacionIncompleta("Debe completar la información, por lo menos \nla placa y el año de fabricació o adquisición.");
        this.placa=placa;
        this.marca=marca;
        this.color=color;
        this.motor=motor;
        this.nomFile=nomFile;
        this.ruedas=Integer.parseInt(ruedas);
        this.año=Integer.parseInt(año);
    }
    
    public String getPlaca() {
        return placa;
    }

    public String getMarca() {
        return marca;
    }

    public String getColor() {
        return color;
    }

    public String getMotor() {
        return motor;
    }

    public int getRuedas() {
        return ruedas;
    }

    public int getAño() {
        return año;
    }

    public Venta getVenta() {
        return venta;
    }

    public Descripcion getDescripcion() {
        return descripcion;
    }

    public String getNomFile() {
        return nomFile;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public void setRuedas(int ruedas) {
        this.ruedas = ruedas;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public void setDescripcion(Descripcion descripcion) {
        this.descripcion = descripcion;
    }

    public void setNomFile(String nomFile) {
        this.nomFile = nomFile;
    }
    
    @Override
    public String toString(){
        return "\n【Placa】: "+this.placa+" 【Marca】: "+this.marca+" 【Color】: "+this.color+"\n【Tipo de motor】: "+this.motor+" 【Número de ruedas】: "+this.ruedas+"\n【Año de fabricación/adquisición del vehículo】: "+this.año;
    }
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Vehiculo other = (Vehiculo)obj;
        return Objects.equals(this.placa,other.placa);
    }

    @Override
    public int compareTo(Vehiculo o) {
        if(this.año < o.getAño())
            return 1;
        else if(this.año > o.getAño())
            return -1;
        else
            return 0;
    }
}
