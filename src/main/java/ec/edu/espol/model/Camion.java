/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Camion extends Descripcion implements Serializable{
    private double capacidadcarga;
    
    public Camion(){}
    
    public Camion(String recorrido, String pasajeros, String espejo, String tipofrenos, String arranque, String modelo,String carga) throws Exception{
        super(recorrido, pasajeros, espejo, tipofrenos, arranque, modelo);
        if(carga.equals("")) throw new InformacionIncompleta("Debe completar la información, le falta\nindicar la capacidad de carga del camión.");
        this.capacidadcarga=Double.parseDouble(carga);
    }

    public double getCapacidadcarga() {
        return capacidadcarga;
    }

    public void setCapacidadcarga(double capacidadcarga) {
        this.capacidadcarga = capacidadcarga;
    }
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipofrenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido+"\n【Capacidad promedio de carga】: "+this.capacidadcarga;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Camion other = (Camion)obj;
        return this.capacidadcarga==other.capacidadcarga;
    }
}
