/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.util.*;
import javafx.scene.control.*;
/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Rol implements Serializable, Comparable<Rol>{
    private String usuario, clave,correo;
    private ArrayList<String> tipousuario;
    private ArrayList<Venta> ventas;
    private Persona persona;
    
    public Rol(TextField stUs,PasswordField stClav,TextField stMail,ArrayList<String> tipo) throws Exception{
        String mail = Sistema.validarCorreo(stMail.getText());
        if(tipo==null) throw new SeleccionIncorrecta("No ha seleccionado una de las opciones");
        this.usuario=stUs.getText();
        this.clave=Sistema.toHexString(Sistema.getSHA(stUs.getText()+stClav.getText()));
        this.correo=mail;
        this.tipousuario=tipo;
    }
    
    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }

    public String getCorreo() {
        return correo;
    }

    public ArrayList<String> getTipousuario() {
        return tipousuario;
    }

    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTipousuario(ArrayList<String> tipousuario) {
        this.tipousuario = tipousuario;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }

    public void setPersona(Persona persona) {
        this.persona= persona;
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.usuario,this.clave,this.correo,this.tipousuario);
    }
    
    @Override
    public String toString(){
        return "【Correo】: "+this.correo;
    }
    
    @Override
    public int compareTo(Rol r){
        if(Objects.equals(this.usuario, r.getUsuario()))
            return 1;
        else if(Objects.equals(this.correo, r.getCorreo()))
            return -1;
        else
            return 0;
    }
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Rol other = (Rol)obj;
        return Objects.equals(this.correo,other.correo);
    }
}