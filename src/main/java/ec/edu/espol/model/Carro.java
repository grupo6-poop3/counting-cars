/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Carro extends Descripcion implements Serializable{
    private String descapotable;
    
    public Carro(){}
    
    public Carro(String recorrido, String pasajeros, String espejo, String tipofrenos, String arranque, String modelo,String descapotable) throws Exception{
        super(recorrido, pasajeros, espejo, tipofrenos, arranque, modelo);
        if(descapotable.equals("NO DATA")) throw new InformacionIncompleta("Debe completar la información, le falta\nescoger si no es descapotable o que tipo.");
        this.descapotable=descapotable;
    }
    public String getDescapotable() {
        return descapotable;
    }

    public void setDescapotable(String descapotable) {
        this.descapotable = descapotable;
    }
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipofrenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido+"\n【Descapotable】: "+this.descapotable;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Carro other = (Carro)obj;
        return this.descapotable==other.descapotable;
    }
}
