/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.util.Properties;
import javafx.application.Platform;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Mail implements Runnable{
    
    private String asunto, correo, mensaje;
    
    public Mail(String asunto,String correo,String mensaje){
        this.asunto=asunto;
        this.correo=correo;
        this.mensaje=mensaje;
    }
    
    @Override
    public void run(){
         Platform.runLater(()->{
             try {
                 enviarMail(correo,asunto,mensaje);
             } catch (Exception ex) {
                 ex.printStackTrace();
             }
         });
    }
    
    public static void enviarMail(String correo, String asunto, String mensaje) throws Exception{
         try (InputStream inputStream = new FileInputStream(new File("archivos.properties"))) {
             Properties prop = new Properties();
             prop.load(inputStream);
             Session session = Session.getDefaultInstance(prop);
             session.setDebug(false);
             MimeMessage message = new MimeMessage(session);
             message.setFrom(new InternetAddress("xxmotorsGuayas@gmail.com"));
             InternetAddress[] x = {new InternetAddress(correo)};
             message.addRecipients(Message.RecipientType.TO, x);
             message.setSubject(asunto);
             message.setText(mensaje);
             Transport t = session.getTransport("smtp");
             t.connect("smtp.gmail.com","countingcars2020G6", "Grupo62020");
             t.sendMessage(message, message.getAllRecipients());
             t.close();
         }
    }
}
