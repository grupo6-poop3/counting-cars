/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;
import java.io.*;
import java.util.*;
import javafx.scene.control.*;
/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Persona implements Serializable,Comparable<Persona>{
    private String nombres,apellidos,organizacion;
    private int cedula;
    private Rol rol;
    
    public Persona(){}
    
    public Persona(String clave){
        this.rol.setClave(clave);
    }
    
    public Persona(String nombres, String apellidos, String organizacion, String cedula){
        this.nombres= nombres;
        this.apellidos= apellidos;
        this.organizacion=organizacion;
        this.cedula=Integer.parseInt(cedula);
    }
    
    public String getNombres() {
        return nombres;
    }
    
    public String getApellidos() {
        return apellidos;
    }
    
    public int getCedula() {
        return cedula;
    }
    
    public String getOrganizacion() {
        return organizacion;
    }
    
    public Rol getRol() {
        return rol;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    
    public void setCedula(int cedula) {
        this.cedula = cedula;
    }
    
    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }
    
    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    public static void guardarPersona(TextField stNombre,TextField stApel,TextField stCed,TextField stOrg,TextField stClav, Rol rol) throws Exception{
        ArrayList<Persona> lista= Persona.desserializarPersona("Persona.ser");
        Persona p = new Persona(stNombre.getText(),stApel.getText(),stOrg.getText(),stCed.getText());
        p.setRol(rol);
        if(Sistema.existeUsuario(p)) throw new UsuarioIncorrecto("Este cuenta ya existe.\nPor favor, ingresar correctamente los datos o inicie sesión");
        lista.add(p);
        Thread th = new Thread(new Mail("Registro de cuenta en Counting Cars",p.getRol().getCorreo(),"Listo! Su cuenta se ha registrado correctamente.Gracias por usar nuestro servicio.\nDatos de la cuenta:\nUsuario: "+p.getRol().getUsuario()+"\tClave: "+stClav.getText()+"\nTipo de cuenta: "+p.getRol().getTipousuario()+"\nAtt,\nCounting Cars"));
        th.start();
        Persona.serializarPersona(lista, "Persona.ser");
        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), null, null, "Se ha registrado correctamente el usuario!");
    }
    
    public static void serializarPersona(ArrayList<Persona> listap, String nombre){
        try(ObjectOutputStream a = new ObjectOutputStream(new FileOutputStream(nombre))){
            a.writeObject(listap);
        }catch(IOException e){}
    }
    
    public static ArrayList<Persona> desserializarPersona(String nombre){
        try(ObjectInputStream a = new ObjectInputStream(new FileInputStream(nombre))){
            return (ArrayList<Persona>) a.readObject();
        }catch(Exception e){
            return new ArrayList<>();
        }
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.nombres,this.apellidos,this.organizacion,this.cedula,this.rol);
    }
    
    @Override
    public String toString(){
        return "\n【Nombres】: "+this.getNombres()+"\n"+this.rol;
    }
    
    @Override
    public int compareTo(Persona x){
        if(this.equals(x))
            return 1;
        else if(Objects.equals(this.nombres,x.getNombres())&&Objects.equals(this.apellidos,x.getApellidos()))
            return -1;
        else
            return 0;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Persona other = (Persona)obj;
        return this.cedula==other.cedula;
    }
}
