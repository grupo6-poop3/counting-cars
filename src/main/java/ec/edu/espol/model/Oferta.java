/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import javafx.scene.control.Alert;
/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Oferta implements Serializable,Comparable<Oferta>{
    private double rebaja;
    private Persona persona;
    private Venta ventas;
    
    public Oferta(){}
    
    public Oferta(String rebaja, Persona persona){
        this.rebaja=Double.parseDouble(rebaja);
        this.persona=persona;
    }
    
    
    public double getRebaja() {
        return rebaja;
    }
    
    public Persona getPersona() {
        return persona;
    }
    
    public Venta getVentas() {
        return ventas;
    }
    
    public void setRebaja(double rebaja) {
        this.rebaja = rebaja;
    }
    
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public void setVentas(Venta ventas) {
        this.ventas = ventas;
    }
    
    public static void ofertar(Oferta oferta, Venta ventaActual, Persona p) throws Exception{
        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION),"Ofertar un vehículo","Oferta guardada","Su precio de oferta para el vehículo de \nplaca "+ventaActual.getVehiculo().getPlaca()+" ha sido guardada en los datos.\nEspere la respuesta del vendedor.");
        String asunto = "Oferta de venta de un vehículo";
        String cuerpo = "Saludos,\n"+p.getRol().getUsuario()+" le ofrece una oferta de: $"+oferta+" para el vehiculo de placa: ";
        Thread th = new Thread(new Mail(asunto,p.getRol().getCorreo(),cuerpo));
        th.start();
        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), null, null,"Se ha enviado su oferta al vendedor");
    }
    
    @Override
    public String toString(){
        return "Oferta: "+this.rebaja+"\n"+this.persona;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Oferta other = (Oferta)obj;
        return this.rebaja==other.rebaja;
    }

    @Override
    public int compareTo(Oferta o) {
        if(this.rebaja < o.getRebaja())
            return 1;
        else if(this.rebaja > o.getRebaja())
            return -1;
        else
            return 0;
    }
}