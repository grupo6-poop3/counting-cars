/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.util.*;
import javafx.scene.control.Alert;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */

public class Venta implements Serializable, Comparable<Venta>{
    private double precio;
    private Vehiculo vehiculo;
    private ArrayList<Oferta> ofertas;
    private Persona persona;
    
    public Venta(double precio){
        this.precio=precio;
    }
    
    public Venta(String precio) throws Exception{
        if(precio.equals("0")) throw new InformacionIncompleta("Debe completar la información requerida.\nAgregue el precio del vehículo a vender.");
        this.precio=Double.parseDouble(precio);
        this.ofertas=new ArrayList<>();
    }
    
    public double getPrecio(){
        return precio;
    }
    
    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public ArrayList<Oferta> getOfertas() {
        if(this.ofertas==null)
            return new ArrayList<>();
        return ofertas;
            
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }
    
    public void setOfertas(Oferta oferta){
        this.ofertas.add(oferta);
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public static void aceptarOferta(String placa, Persona p, Oferta o) throws Exception{
        ArrayList<Venta> listaVenta = desserializarVenta("Ventas.ser");
        for(Venta v: listaVenta){
            if(v!=null){
                if(v.getVehiculo().getPlaca().equals(placa)){
                    Thread th = new Thread(new Mail("Oferta aceptada",p.getRol().getCorreo(),"El vendedor "+p.getNombres()+" espera su respuesta para procesar la venta.\nCorreo para comunicarse: "+p.getRol().getCorreo()+"\nAtt,\nCounting Cars"));
                    th.start();
                    listaVenta.remove(v);
                    serializarVenta(listaVenta,"Ventas.ser");
                    Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), null, null, "¡Correo enviado y Felicitacions por su venta!\nSe ha enviado un correo con sus datos para\nque se contacte con usted en un perido de 72 horas\n");
                }
            }
        }
    }
    
    public static void serializarVenta(ArrayList<Venta> listap, String nombre){
        try(ObjectOutputStream a = new ObjectOutputStream(new FileOutputStream(nombre))){
            a.writeObject(listap);
        }catch(IOException e){}
    }
    
    public static ArrayList<Venta> desserializarVenta(String nombre){
        try(ObjectInputStream a = new ObjectInputStream(new FileInputStream(nombre))){
            return (ArrayList<Venta>) a.readObject();
        }catch(Exception e){
            return new ArrayList<>();
        }
    }
    
    public static ArrayList<Venta> criterioTipoVehiculo(ArrayList<Venta> vehiculos,String tipo) throws InformacionIncompleta{
        ArrayList<Venta> mostrar = new ArrayList<>();
        for(Venta v : vehiculos){
            if(tipo.equals("Moto")&&(v.getVehiculo().getDescripcion() instanceof Moto)) mostrar.add(v);
            else if(tipo.equals("Camión")&&(v.getVehiculo().getDescripcion() instanceof Camion)) mostrar.add(v);
            else if(tipo.equals("Camioneta")&&(v.getVehiculo().getDescripcion() instanceof Camioneta)) mostrar.add(v);
            else if(tipo.equals("Furgoneta")&&(v.getVehiculo().getDescripcion() instanceof Furgoneta)) mostrar.add(v);
            else if(tipo.equals("Carro")&&(v.getVehiculo().getDescripcion() instanceof Carro)) mostrar.add(v);
            else if(tipo.equals("Bus")&&(v.getVehiculo().getDescripcion() instanceof Bus)) mostrar.add(v);
            else if(tipo.equals("Todos")) return vehiculos;
        }
        return mostrar;
    }
    
    public static ArrayList<Venta> criterioPrecio(ArrayList<Venta> vehiculos,String precioInicial, String precioFinal) throws Exception{
        if(Double.parseDouble(precioInicial)<0||Double.parseDouble(precioFinal)<=0||Double.parseDouble(precioInicial)>=Double.parseDouble(precioFinal)) throw new ValorFueraRango("El rango de precio que ha ingresado no está \npermitido.\nInsertar valores mayores a cero");
        ArrayList<Venta> mostrar = new ArrayList<>();
        for(Venta v : vehiculos){
            if(v.getPrecio()>=Double.parseDouble(precioInicial)&&v.getPrecio()<=Double.parseDouble(precioFinal)) mostrar.add(v);
        }
        return mostrar;
    }
    
    public static ArrayList<Venta> criterioAnual(ArrayList<Venta> vehiculos,String añoInicial, String añoFinal) throws Exception{
        if(Integer.parseInt(añoInicial)<0||Integer.parseInt(añoFinal)<=0||Integer.parseInt(añoInicial)>=Integer.parseInt(añoFinal)) throw new ValorFueraRango("El rango de año que ha ingresado no está \npermitido.\nInsertar valores mayores a cero");
        ArrayList<Venta> mostrar = new ArrayList<>();
        for(Venta v : vehiculos){
            if(v.getVehiculo().getAño()>=Integer.parseInt(añoInicial)&&v.getVehiculo().getAño()<=Integer.parseInt(añoFinal)) mostrar.add(v);
        }
        return mostrar;
    }
    
    public static ArrayList<Venta> criterioRecorrido(ArrayList<Venta> vehiculos,String recInicial, String recFinal) throws Exception{
        if(Integer.parseInt(recInicial)<0||Integer.parseInt(recFinal)<=0||Integer.parseInt(recInicial)>=Integer.parseInt(recFinal)) throw new ValorFueraRango("El rango de recorrido que ha ingresado no está \npermitido.\nInsertar valores mayores a cero");
        ArrayList<Venta> mostrar = new ArrayList<>();
        for(Venta v : vehiculos){
            if(v.getVehiculo().getDescripcion().getRecorrido()>=Integer.parseInt(recInicial)&&v.getVehiculo().getDescripcion().getRecorrido()<=Integer.parseInt(recFinal)) mostrar.add(v);
        }
        return mostrar;
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.precio,this.persona,this.vehiculo,this.ofertas);
    }
    
    @Override
    public String toString(){
        return "【Precio】: "+this.precio+"\n"+this.persona+"\n"+this.vehiculo;
    }
    
    @Override
    public int compareTo(Venta x){
        if(this.precio < x.getPrecio())
            return 1;
        else if(this.precio > x.getPrecio())
            return -1;
        else
            return 0;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Venta other = (Venta)obj;
        return this.precio==other.precio;
    }
}
