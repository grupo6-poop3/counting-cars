/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Moto extends Descripcion implements Serializable{
    private String tipotimon;
    
    public Moto(){}
    
    public Moto(String recorrido, String pasajeros, String espejo, String tipofrenos, String arranque, String modelo,String tipotimon) throws Exception{
        super(recorrido, pasajeros, espejo, tipofrenos, arranque, modelo);
        if(tipotimon.equals("NO DATA")) throw new InformacionIncompleta("Debe completar la información, le falta\nescoger que tipo de timon tiene la moto.");
        this.tipotimon=tipotimon;
    }

    public String getTipotimon() {
        return tipotimon;
    }

    public void setTipotimon(String tipotimon) {
        this.tipotimon = tipotimon;
    }
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipofrenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido+"\n【Tipo de timón】: "+this.tipotimon;
    }
    
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Moto other = (Moto)obj;
        return this.tipofrenos.equals(other.tipofrenos);
    }
}
