/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;
/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Descripcion implements Serializable,Comparable<Descripcion>{
    protected int recorrido, pasajeros, espejo;
    protected String tipo_frenos, transmision, modelo;
    protected Vehiculo vehiculo;
    
    public Descripcion(){}
    
    public Descripcion(String recorrido, String pasajeros, String espejo, String tipo_frenos, String transmision, String modelo) throws Exception{
        this.recorrido = Integer.parseInt(recorrido);
        this.pasajeros= Integer.parseInt(pasajeros);
        this.espejo=Integer.parseInt(espejo);
        if(transmision.equals("NO DATA")) throw new InformacionIncompleta("Debe completar la información, por lo menos \nla placa y el año de fabricació o adquisición.");
        this.tipo_frenos=tipo_frenos;
        this.transmision=transmision;
        this.modelo=modelo;
    }

    public int getRecorrido() {
        return recorrido;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public int getEspejo() {
        return espejo;
    }

    public String getTipo_frenos() {
        return tipo_frenos;
    }

    public String getTransmision() {
        return transmision;
    }

    public String getModelo() {
        return modelo;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setRecorrido(int recorrido) {
        this.recorrido = recorrido;
    }

    public void setPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }

    public void setEspejo(int espejo) {
        this.espejo = espejo;
    }

    public void setTipo_frenos(String tipo_frenos) {
        this.tipo_frenos = tipo_frenos;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipo_frenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Descripcion other = (Descripcion)obj;
        return this.recorrido==other.recorrido && this.transmision.equals(other.transmision);
    }    

    @Override
    public int compareTo(Descripcion o) {
        if(this.recorrido < o.getRecorrido())
            return 1;
        else if(this.recorrido > o.getRecorrido())
            return -1;
        else
            return 0;
    }
}