/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Camioneta extends Descripcion implements Serializable{
    private String mecanismoventana;
    
    
    public Camioneta(){}
    
    public Camioneta(String recorrido, String pasajeros, String espejo, String tipofrenos, String arranque, String modelo,String mecanismo) throws Exception{
        super(recorrido, pasajeros, espejo, tipofrenos, arranque, modelo);
        if(mecanismo.equals("NO DATA")) throw new InformacionIncompleta("Debe completar la información, le falta\nescoger el tipo de mecanismo para subir el\nvidrio.");
        this.mecanismoventana=mecanismo;
    }

    public String getMecanismoventana() {
        return mecanismoventana;
    }

    public void setMecanismoventana(String mecanismo_ventana) {
        this.mecanismoventana = mecanismo_ventana;
    }
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipofrenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido+"\n【Mecanismo para el control de las ventanas】: "+this.mecanismoventana;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Camioneta other = (Camioneta)obj;
        return this.mecanismoventana==other.mecanismoventana;
    }
}
