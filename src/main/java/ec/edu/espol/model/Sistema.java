/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import ec.edu.espol.controller.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.*;
import java.util.regex.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.*;

/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Sistema {
    
    private Sistema(){}
    
    public static void guardarImagenes(ArrayList<Image> listI){
        try(ObjectOutputStream a = new ObjectOutputStream(new FileOutputStream("Coleccion de Imagenes.ser"))){
            a.writeObject(listI);
        }catch(IOException e){}
    }
    public static ArrayList<Image> cargarImagenes() throws ClassNotFoundException{
        try(ObjectInputStream a = new ObjectInputStream(new FileInputStream("Coleccion de Imagenes.ser"))){
            return (ArrayList<Image>) a.readObject();
        }catch(IOException e){
            return new ArrayList<>();
        }
    }
    
    public static void contactoAyuda(Persona persona) throws IOException{
       FXMLLoader loader= Sistema.loadFXML("contactoAyuda");
       Parent root= loader.load();
       ContactoAyudaController controlador= loader.getController();
       controlador.setPersona(persona);
       Scene scene=new Scene(root);
       Stage stage= new Stage();
       stage.initModality(Modality.APPLICATION_MODAL);
       stage.initStyle(StageStyle.UNDECORATED);
       stage.setScene(scene);
       stage.showAndWait();
    }
    
    public static void showMessage(Alert show,String titulo, String encabezado, String mensaje){
        show.setHeaderText(encabezado);
        show.setTitle(titulo);
        show.setContentText(mensaje);
        show.showAndWait();
    }
    
    public static FXMLLoader loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader;
    }
    
    public static String llenarTexto(TextField texto){
        if(texto.getText().equals(""))
            return "NO DATA";
        return texto.getText();
    }
    
    public static String llenarNum(TextField texto){
        if(texto.getText().equals(""))
            return "0";
        return texto.getText();
    }
    
    public static void agregarVehiculoArchivo(Venta p) throws IOException{
        ArrayList<Venta> listaP= Venta.desserializarVenta("Ventas.ser");
        listaP.add(p);
        Venta.serializarVenta(listaP, "Ventas.ser");
        Alert m = new Alert(Alert.AlertType.INFORMATION);
        showMessage(m, "Datos de vehículo","Venta de vehículo","El vehículo "+p.getVehiculo().getMarca()+"se ha agregado a la venta sin ningún problema");
    }
    
    public static String llenarDatosdelComboBox(ComboBox box){
        if(box.getValue()==null)
            return "NO DATA";
        return box.getValue().toString();
    }
    
    
    public static byte[] getSHA(String input) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(input.getBytes(StandardCharsets.UTF_8));  
    }

    public static String toHexString(byte[] hash){
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));
        while (hexString.length() < 32){  
            hexString.insert(0, '0');  
        }
        return hexString.toString();  
    }
    
    public static boolean existeUsuario(Persona per){
        ArrayList<Persona> users = Persona.desserializarPersona("Persona.ser");
        for(Persona user: users){
            if(user.getRol().equals(per.getRol()))
                return true;
        }
        return false;
    }
    
    public static boolean verificarCuenta(String sesion) throws NoSuchAlgorithmException{
        try{
            ArrayList<Persona> codClaves = Persona.desserializarPersona("Persona.ser");
            for(Persona e: codClaves){
                if(Sistema.toHexString(Sistema.getSHA(sesion)).equals(e.getRol().getClave()))
                    return true;
            }
            return false;
        }catch(Exception e){
            return false;
        }
    }
    
    public static boolean verificarPlaca(String placa){
        ArrayList<Venta> listaV = Venta.desserializarVenta("Ventas.ser");
        for(Venta e: listaV)
            if(e.getVehiculo().getPlaca().equals(placa))
                return true;
        return false;
    }
    
    public static String validarCorreo(String email) throws CorreoIncorrecto{
        Pattern pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");   
        Matcher mat = pat.matcher(email);
        if(!mat.find()) throw new CorreoIncorrecto("No ha ingresado correctamente el correo.\nAsegúrese de ingresar un correo existente.");
        return email;
    }
    
    public static void eliminarArchivo(Alert alrt,String archivo, String titulo, String mensaje, String error){
        if((new File(archivo)).exists()){
            if((new File(archivo)).delete()){
                showMessage(alrt, titulo, null, mensaje);
            }
        }else Sistema.showMessage(new Alert(Alert.AlertType.ERROR),titulo,null,error);
    }
}