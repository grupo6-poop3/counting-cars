/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Furgoneta extends Descripcion implements Serializable{
    private int asientos;
    
    public Furgoneta(){}
    
    public Furgoneta(String recorrido, String pasajeros, String espejo, String tipo_frenos, String arranque, String modelo,String asientos) throws Exception{
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        if(asientos.equals("")) throw new InformacionIncompleta("Debe completar la información, le falta\nindicar la cantidad de asientos de la furgoneta.");
        this.asientos=Integer.parseInt(asientos);
    }

    public int getAsientos() {
        return asientos;
    }

    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipofrenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido+"\n【Velocidad promedio】: "+this.asientos;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Furgoneta other = (Furgoneta)obj;
        return this.asientos==other.asientos;
    }
}
