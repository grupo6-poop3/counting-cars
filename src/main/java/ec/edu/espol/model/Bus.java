/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class Bus extends Descripcion implements Serializable{
    private int vidrios;
    
    public Bus(){}
    
    public Bus(String recorrido, String pasajeros, String espejo, String tipofrenos, String arranque, String modelo,String vidrios) throws Exception{
        super(recorrido, pasajeros, espejo, tipofrenos, arranque, modelo);
        if(vidrios==null) throw new InformacionIncompleta("Debe completar por lo menos la información marcada.\n(Placa, Año, Tipo de motor, transmisión y la opción \nespecífica del vehículo a ingresar)");
        this.vidrios=Integer.parseInt(vidrios);
    }
    
    public int getVidrios() {
        return vidrios;
    }

    public void setVidrios(int vidrios) {
        this.vidrios = vidrios;
    }
    
    
    @Override
    public String toString(){
        return "\n【Transmisión del vehículo】: "+this.transmision+" 【Modelo】: "+this.modelo+" 【Sistema de frenado】: "+this.tipofrenos+"\n【Número de espejos】: "+this.espejo+" 【Número de pasajeros】: "+this.pasajeros+" 【Kilometraje del carro】: "+this.recorrido+"\n【Número de ventanas】: "+this.vidrios;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Bus other = (Bus)obj;
        return this.vidrios==other.vidrios;
    }
}