/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.*;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author thund
 */
public class PerfilController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private TextField usuario;
    @FXML
    private TextField nombres;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField correo;
    @FXML
    private TextField org;
    @FXML
    private TextField cedula;
    @FXML
    private PasswordField contra;
    @FXML
    private PasswordField oldPass;
    @FXML
    private Text titulo;
    @FXML
    private ComboBox tipo;
    
    private Persona persona;
    private ArrayList<String> listS = new ArrayList<>();
    private ArrayList<Persona> listP;
    private int indice;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    public void recuperarDatosP(Persona per){
        this.persona = per;
        listP = Persona.desserializarPersona("Persona.ser");
        for(Persona user : listP){
            if(Objects.equals(user,persona)) indice = listP.indexOf(user);
        }
        usuario.setText(persona.getRol().getUsuario());
        nombres.setText(persona.getNombres());
        apellidos.setText(persona.getApellidos());
        correo.setText(persona.getRol().getCorreo());
        org.setText(persona.getOrganizacion());
        cedula.setText(String.valueOf(persona.getCedula()));
        asignarTipo();
        tipo.setItems(FXCollections.observableArrayList(listS));
        titulo.setText("Perfil de "+persona.getNombres());
    }
    
    private void asignarTipo(){
        if(persona.getRol().getTipousuario().size()==2){
            tipo.setPromptText("Vendedor/Comprador");
            listS.add("Vendedor");
            listS.add("Comprador");
        }
        else{
            rolSeleccionado();
        }
    }

    private void rolSeleccionado(){
        switch (persona.getRol().getTipousuario().get(0)) {
            case "Comprador":
                tipo.setPromptText(persona.getRol().getTipousuario().get(0));
                listS.add("Vendedor");
                listS.add("Vendedor/Comprador");
                break;
            case "Vendedor":
                tipo.setPromptText(persona.getRol().getTipousuario().get(0));
                listS.add("Comprador");
                listS.add("Vendedor/Comprador");
                break;
            default:
                break;
        }}
    
    @FXML
    public void cambiarR(ActionEvent event){
        ArrayList<String> strs = new ArrayList<>();
        if(tipo.getValue().toString().equals("Vendedor/Comprador")){
            strs.add("Vendedor");
            strs.add("Comprador");
            listP.get(indice).getRol().setTipousuario(strs);
        }
        else if(tipo.getValue().toString().equals("Vendedor")){
            strs.add("Vendedor");
            listP.get(indice).getRol().setTipousuario(strs);
        }
        else if(tipo.getValue().toString().equals("Comprador")){
            strs.add("Comprador");
            listP.get(indice).getRol().setTipousuario(strs);
        }cambiandoRol();
    }
    
    public void cambiandoRol(){
        try{
            Persona.serializarPersona(listP, "Persona.ser");
            Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Cambio de rol", null, "Se ha cambiado de rol correctamente");
            Thread th = new Thread(new Mail("Cambio de rol",persona.getRol().getCorreo(), "Se ha cambiado de rol correctamente.\nUsuario: "+persona.getRol().getUsuario()+"\tNuevo rol: "+tipo.getValue().toString()));
            th.start();
            if(listP.get(indice).getRol().getTipousuario().size()==2) pasarVC();
            else if(Objects.equals(listP.get(indice).getRol().getTipousuario().get(0),"Vendedor")) pasarV();
            else if(Objects.equals(listP.get(indice).getRol().getTipousuario().get(0),"Comprador")) pasarC();
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Cambio de rol", null, "Al parecer hubo problemas para cambiar el rol de usuario");
        }
    }
    
    private void pasarVC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedorycomprador");
        Parent form = vc.load();
        VendedorycompradorController vcc = vc.getController();
        persona = listP.get(indice);
        vcc.recuperarDatosVC(persona);
        App.setRoot(form);
    }
    
    private void pasarV() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedor");
        Parent form = vc.load();
        VendedorController vcc = vc.getController();
        persona = listP.get(indice);
        vcc.recuperarDatosV(persona);
        App.setRoot(form);
    }
    
    private void pasarC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("comprador");
        Parent form = vc.load();
        CompradorController vcc = vc.getController();
        persona = listP.get(indice);
        vcc.recuperarDatosC(persona);
        App.setRoot(form);
    }
    
    @FXML
    public void cambiarC(KeyEvent event){
        try{
            if(event.getCode()==KeyCode.ENTER){
                if(contra.getText().equals("")||oldPass.getText().equals("")) throw new InformacionIncompleta("Debe escribir ambas contraseñas");
                else if(!Sistema.toHexString(Sistema.getSHA(usuario.getText()+oldPass.getText())).equals(listP.get(indice).getRol().getClave())) throw new InformacionIncompleta("La antigua contraseña ingresada no coindice");
                    listP.get(indice).getRol().setClave(Sistema.toHexString(Sistema.getSHA(usuario.getText()+contra.getText())));
                    Persona.serializarPersona(listP, "Persona.ser");
                    Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Cambio de contraseña", null, "Se ha cambiado de contraseña correctamente");
                    Thread th = new Thread(new Mail("Cambio de contraseña",persona.getRol().getCorreo(), "Se ha cambiado de contraseña correctamente.\nUsuario: "+persona.getRol().getUsuario()+"\tNueva Contraseña: "+contra.getText()));
                    th.start();
                    if(listP.get(indice).getRol().getTipousuario().size()==2) pasarVC();
                    else if(Objects.equals(listP.get(indice).getRol().getTipousuario().get(0),"Vendedor")) pasarV();
                    else if(Objects.equals(listP.get(indice).getRol().getTipousuario().get(0),"Comprador")) pasarC();
        }}catch(InformacionIncompleta e){Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Cambio de contraseña", null, e.getMessage());}
        catch(Exception e){Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Cambio de contraseña", null, "Al parecer hubo problemas para cambiar la contraseña");}
    }
    
    @FXML
    public void cambiarC1(MouseEvent event){
        try{
            if(contra.getText().equals("")||oldPass.getText().equals("")) throw new InformacionIncompleta("Debe escribir ambas contraseñas");
            else if(!Sistema.verificarCuenta(listP.get(indice).getRol().getUsuario()+oldPass.getText())) throw new InformacionIncompleta("La antigua contraseña ingresada no coindice");
                listP.get(indice).getRol().setClave(Sistema.toHexString(Sistema.getSHA(usuario.getText()+contra.getText())));
                Persona.serializarPersona(listP, "Persona.ser");
                Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Cambio de contraseña", null, "Se ha cambiado de contraseña correctamente");
                Thread th = new Thread(new Mail("Cambio de contraseña",persona.getRol().getCorreo(), "Se ha cambiado de contraseña correctamente.\nUsuario: "+persona.getRol().getUsuario()+"\tNueva Contraseña: "+contra.getText()));
                th.start();
                if(listP.get(indice).getRol().getTipousuario().size()==2) pasarVC();
                else if(Objects.equals(listP.get(indice).getRol().getTipousuario().get(0),"Vendedor")) pasarV();
                else if(Objects.equals(listP.get(indice).getRol().getTipousuario().get(0),"Comprador")) pasarC();
        }catch(InformacionIncompleta e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Cambio de contraseña", null, e.getMessage());
        }catch(Exception e){Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Cambio de contraseña", null, "Al parecer hubo problemas para cambiar la contraseña");}
    }
    
    @FXML
    public void regresar(MouseEvent event) throws IOException{
        if(persona.getRol().getTipousuario().size()==2) pasarVC();
        else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Vendedor")) pasarV();
        else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Comprador")) pasarC();
    }
    
    
    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }
    
    @FXML
    private void guia(ActionEvent event) {
        try {
            Sistema.contactoAyuda(persona);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
