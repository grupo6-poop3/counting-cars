/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;
import ec.edu.espol.model.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
/**
 * FXML Controller class
 * 
 *@author Freddy Tenesaca
 * @author Andres Medina
 * @author Joaquin Rivadeneira
 */
public class ContactoAyudaController implements Initializable {
    @FXML
    private TextField txt_Asunto;
    @FXML
    private TextArea txt_Mensaje;
    private Persona persona;
    @FXML
    private Button bte;
    @FXML
    private Button btc;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
        // TODO
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    private void enviarCorreo(MouseEvent event) throws Exception {
        String mens=txt_Mensaje.getText()+"\nUsuario:"+persona.getRol().getUsuario()+"Correo:"+persona.getRol().getCorreo();
        Mail.enviarMail("countingcars2020g6@gmail.com",txt_Asunto.getText(),mens);
        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), null, null, "Se ha enviado el correo correctamente");
        Stage stage=(Stage)this.bte.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void borrar(MouseEvent event) {
        Stage stage=(Stage)this.btc.getScene().getWindow();
        stage.close();
    }
}
