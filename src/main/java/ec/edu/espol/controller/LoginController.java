/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.*;
/**
 * FXML Controller class
 *@author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class LoginController implements Initializable {


    @FXML
    private TextField txUsIn;
    @FXML
    private TextField txClIn;
    private Persona persona;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        txClIn.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches(".{0,15}"))? condicion: null ));
        txUsIn.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("\\w{0,15}"))? condicion: null ));
    }
    
    @FXML
    public void regresar(MouseEvent event) throws IOException{
        App.setRoot("MenuPrincipal");
    }
    
    @FXML
    public void nuevoUser(MouseEvent event) throws IOException{
        App.setRoot("registroPersona");
    }
    
    @FXML
    public void iniciarSesion(MouseEvent event){
        try{
            if(Sistema.verificarCuenta(txUsIn.getText()+txClIn.getText())){
                for(Persona p: Persona.desserializarPersona("Persona.ser")){
                    if(p.getRol().getClave().equals(Sistema.toHexString(Sistema.getSHA(txUsIn.getText()+txClIn.getText())))){
                        this.persona = p;
                        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Sesión", "Sesión iniciada", "Los datos fueron correctamente cargados\nNombre Completo: "+p.getNombres()+" "+p.getApellidos()+"\nTipo de usuario: "+p.getRol().getTipousuario());
                        if(p.getRol().getTipousuario().size()==2) asignarOpcion("vendedorycomprador");
                        else asignarOpcion(p.getRol().getTipousuario().get(0));
                    }
                }
            }else{Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Sesión", "Error al iniciar", "No existe un usuario con esos datos.\nIngrese correctamente los datos");}
        }catch(Exception e){Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Sesión", "Error al iniciar", "ERROR EN EL SISTEMA");}
    }
    
    @FXML
    private void login(KeyEvent event){
        if(event.getCode()==KeyCode.ENTER){
            try{
            if(Sistema.verificarCuenta(txUsIn.getText()+txClIn.getText())){
                for(Persona p: Persona.desserializarPersona("Persona.ser")){
                    if(p.getRol().getClave().equals(Sistema.toHexString(Sistema.getSHA(txUsIn.getText()+txClIn.getText())))){
                        this.persona = p;
                        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Sesión", "Sesión iniciada", "Los datos fueron correctamente cargados\nNombre Completo: "+p.getNombres()+" "+p.getApellidos()+"\nTipo de usuario: "+p.getRol().getTipousuario());
                        if(p.getRol().getTipousuario().size()==2) asignarOpcion("vendedorycomprador");
                        else asignarOpcion(p.getRol().getTipousuario().get(0));
                    }
                }
            }else{Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Sesión", "Error al iniciar", "No existe un usuario con esos datos.\nIngrese correctamente los datos");}
        }catch(Exception e){Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Sesión", "Error al iniciar", "ERROR EN EL SISTEMA");}
        }
    }
    
    private void asignarOpcion(String p) throws IOException{
        if(Objects.equals(p,"vendedorycomprador")){
            pasarVC();
        }
        else if(Objects.equals(p,"Vendedor")){
            pasarV();
        }
        else if(Objects.equals(p,"Comprador")){
            pasarC();
        }
    }
    
    private void pasarVC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedorycomprador");
        Parent form = vc.load();
        VendedorycompradorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosVC(persona);
    }
    
    private void pasarV() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedor");
        Parent form = vc.load();
        VendedorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosV(persona);
    }
    
    private void pasarC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("comprador");
        Parent form = vc.load();
        CompradorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosC(persona);
    }
    
    @FXML
    private void borrarU(ActionEvent event){
        Sistema.eliminarArchivo(new Alert(Alert.AlertType.INFORMATION),"Persona.ser", "Archivo de Usuarios", "Los datos del archivo Usuarios han sido borrados","No se ha podido borrar los datos");
    }
    
    @FXML
    private void borrarV(ActionEvent event){
        Sistema.eliminarArchivo(new Alert(Alert.AlertType.INFORMATION),"Ventas.ser", "Archivo de Vehículos", "Los datos del archivo Vehiculos han sido borrados","No se ha podido borrar los datos");
    }
    
    
    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona p) {
        this.persona = p;
    }
}
