/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import javafx.fxml.*;
import javafx.scene.input.MouseEvent;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
/**
 * FXML Controller class
 *
 * @author Freddy Gilmar
 * @author Andres Medina
 * @author Joaquin Rivadeneira
 */
public class VenderVController implements Initializable {


    @FXML
    private ComboBox tipoV,motor,transmision,extraIn;
    @FXML
    private Text titulo;
    @FXML
    private TextField placa,marca,color,ruedas,year,recorrido,espejos,frenos,pasajeros,modelo,txExtraIn,precio;
    @FXML
    private Button btnRegistrar;
    
    private File imageFile;
    private Vehiculo v;
    private Venta p;
    private Descripcion c;
    private ArrayList<String> ol;
    private Path origen,destino;
    private Persona persona;
    
    @FXML
    private Label usuario;
    @FXML
    private Label clave;
    @FXML
    private Label correo;
    @FXML
    private Label tipo;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        añadirBoxTipo();
        añadirBoxTransmision();
        añadirBoxMotor();
        btnRegistrar.setDisable(true);
        restringirNumero(ruedas,year,recorrido,espejos,pasajeros,txExtraIn,precio);
        restringirTexto(placa,marca,color,frenos,modelo);
    }    
    
    public static void restringirNumero(TextField ruedas,TextField year,TextField recorrido,TextField espejos,TextField pasajeros,TextField txExtraIn,TextField precio){
        ruedas.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[2-4]{0,1}"))? condition: null));
        year.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[1-2]{0,1}[0-9]{0,3}"))? condition: null));
        recorrido.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[0-9]{0,7}"))? condition: null));
        espejos.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[2-6]"))? condition: null));
        txExtraIn.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[1-9]{0,1}[0-9]{0,7}[.]{0,1}[0-9]{0,2}"))? condition: null));
        precio.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[1-9]{0,1}[0-9]{0,7}[.]{0,1}[0-9]{0,2}"))? condition: null));
    }
    
    public static void restringirTexto(TextField placa,TextField marca,TextField color,TextField frenos,TextField modelo){
        placa.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[a-zA-Z]{0,3}[-]?[0-9]{0,3}"))? condition: null));
        marca.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[a-zA-Z]*"))? condition: null));
        color.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[a-zA-Z]*"))? condition: null));
        frenos.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[a-zA-Z]*"))? condition: null));
        modelo.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[a-zA-Z1-9]*"))? condition: null));
    }
    
    public void recuperarDatosVC(Persona per){
        this.persona = per;
    }
    
    private void añadirBoxTipo(){
        ArrayList<String> listaTipo = new ArrayList<>();
        listaTipo.add("Carro");
        listaTipo.add("Moto");
        listaTipo.add("Camión");
        listaTipo.add("Camioneta");
        listaTipo.add("Bus");
        listaTipo.add("Furgoneta");
        tipoV.setItems(FXCollections.observableArrayList(listaTipo));
    }
    
    private void añadirBoxTransmision(){
        ArrayList<String> listaT=new ArrayList<>();
        listaT.add("Manual");
        listaT.add("Automatizada o secuencial");
        listaT.add("Automático");
        listaT.add("Automatizada con doble embrague");
        listaT.add("CVT");
        transmision.setItems(FXCollections.observableArrayList(listaT));
    }
    
    private void añadirBoxMotor(){
        ArrayList<String> listam=new ArrayList<>();
        listam.add("Mecánico");
        listam.add("Eléctrico");
        listam.add("Híbrido");
        motor.setItems(FXCollections.observableArrayList(listam));
    }
    
    @FXML
    private void registrarV(MouseEvent event){
        try{
            agregarTipoVehiculo(tipoV.getValue().toString());
            if(persona.getRol().getTipousuario().size()==2) pasarVC();
            else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Vendedor")) pasarV();
        }catch(VehicleException | InformacionIncompleta i){
            Sistema.showMessage(new Alert(Alert.AlertType.WARNING), "Registro de vehículo", "Error", i.getMessage()+"\nIngresar correctamente los datos.");
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Registro de vehículo", "Error", "Hubo problemas al registrarse, procure \nIngresar correctamente los datos.");
        }finally{
            extraIn.setValue("");
            txExtraIn.setText("");
            btnRegistrar.setDisable(true);
        }
    }
    
    private void agregarTipoVehiculo(String dato) throws Exception{
        if(Objects.equals(dato,"Carro"))
            venderCarro();
        else if(Objects.equals(dato,"Moto"))
            venderMoto();
        else if(Objects.equals(dato,"Camión"))
            venderCamion();
        else if(Objects.equals(dato,"Camioneta"))
            venderCamioneta();
        else if(Objects.equals(dato,"Bus"))
            venderBus();
        else if(Objects.equals(dato,"Furgoneta"))
            venderFurgoneta();
    }
    
    @FXML
    private void especificarTipo(ActionEvent event){
        if(Objects.equals(tipoV.getValue().toString(),"Carro")) pedirCarro();
        else if(Objects.equals(tipoV.getValue().toString(),"Moto")) pedirMoto();
        else if(Objects.equals(tipoV.getValue().toString(),"Bus")) pedirBus();
        else if(Objects.equals(tipoV.getValue().toString(),"Camión")) pedirCamion();
        else if(Objects.equals(tipoV.getValue().toString(),"Camioneta")) pedirCamioneta();
        else if(Objects.equals(tipoV.getValue().toString(),"Furgoneta")) pedirFurgoneta();
        else 
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Selección", "Error de selección", "Ha ocurrido un problema al escoger un tipo de vehículo");
    }
    
    private void venderCarro() throws Exception{
        if(!Sistema.verificarPlaca(placa.getText())){
            p=new Venta(Sistema.llenarNum(precio));
            v = new Vehiculo(placa.getText(), Sistema.llenarTexto(marca), Sistema.llenarTexto(color),Sistema.llenarDatosdelComboBox(motor), Sistema.llenarNum(ruedas), Sistema.llenarNum(year),imageFile.getName());
            c = new Carro(Sistema.llenarNum(recorrido), Sistema.llenarNum(pasajeros), Sistema.llenarNum(espejos), Sistema.llenarTexto(frenos), Sistema.llenarDatosdelComboBox(transmision), Sistema.llenarTexto(modelo), Sistema.llenarDatosdelComboBox(extraIn));
            v.setDescripcion(c);
            p.setVehiculo(v);
            p.setPersona(persona);
            Files.copy(origen, destino, REPLACE_EXISTING);
            Sistema.agregarVehiculoArchivo(p);
        }else throw new VehicleException("Ya existe un vehículo con la placa que\ningresó...");
    }
    
    private void venderMoto() throws Exception{
        if(!Sistema.verificarPlaca(placa.getText())){
            p=new Venta(precio.getText());
            v = new Vehiculo(placa.getText(), Sistema.llenarTexto(marca), Sistema.llenarTexto(color),Sistema.llenarDatosdelComboBox(motor), Sistema.llenarNum(ruedas), Sistema.llenarNum(year),imageFile.getName());
            c = new Moto(Sistema.llenarNum(recorrido), Sistema.llenarNum(pasajeros), Sistema.llenarNum(espejos), Sistema.llenarTexto(frenos), Sistema.llenarDatosdelComboBox(transmision), Sistema.llenarTexto(modelo), Sistema.llenarDatosdelComboBox(extraIn));
            v.setDescripcion(c);
            p.setVehiculo(v);
            p.setPersona(persona);
            Files.copy(origen, destino, REPLACE_EXISTING);
            Sistema.agregarVehiculoArchivo(p);
        }else Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Vender vehículo", null, "Ya existe un vehículo con la placa que\ningresó...");
    }
    
    private void venderBus()throws Exception{
        if(!Sistema.verificarPlaca(placa.getText())){
            p=new Venta(precio.getText());
            v = new Vehiculo(placa.getText(), Sistema.llenarTexto(marca), Sistema.llenarTexto(color),Sistema.llenarDatosdelComboBox(motor), Sistema.llenarNum(ruedas), Sistema.llenarNum(year),imageFile.getName());
            c = new Bus(Sistema.llenarNum(recorrido), Sistema.llenarNum(pasajeros), Sistema.llenarNum(espejos), Sistema.llenarTexto(frenos), Sistema.llenarDatosdelComboBox(transmision), Sistema.llenarTexto(modelo), txExtraIn.getText());
            v.setDescripcion(c);
            p.setVehiculo(v);
            p.setPersona(persona);
            Files.copy(origen, destino, REPLACE_EXISTING);
            Sistema.agregarVehiculoArchivo(p);
        }else Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Vender vehículo", null, "Ya existe un vehículo con la placa que\ningresó...");
    }
    
    private void venderCamion() throws Exception{
        if(!Sistema.verificarPlaca(placa.getText())){
            p=new Venta(precio.getText());
            v = new Vehiculo(placa.getText(), Sistema.llenarTexto(marca), Sistema.llenarTexto(color),Sistema.llenarDatosdelComboBox(motor), Sistema.llenarNum(ruedas), Sistema.llenarNum(year),imageFile.getName());
            c = new Camion(Sistema.llenarNum(recorrido), Sistema.llenarNum(pasajeros), Sistema.llenarNum(espejos), Sistema.llenarTexto(frenos), Sistema.llenarDatosdelComboBox(transmision), Sistema.llenarTexto(modelo), txExtraIn.getText());
            v.setDescripcion(c);
            p.setVehiculo(v);
            p.setPersona(persona);
            Files.copy(origen, destino, REPLACE_EXISTING);
            Sistema.agregarVehiculoArchivo(p);
        }else Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Vender vehículo", null, "Ya existe un vehículo con la placa que\ningresó...");
    }
    
    private void venderCamioneta() throws Exception{
        if(!Sistema.verificarPlaca(placa.getText())){
            p=new Venta(precio.getText());
            v = new Vehiculo(placa.getText(), Sistema.llenarTexto(marca), Sistema.llenarTexto(color),Sistema.llenarDatosdelComboBox(motor), Sistema.llenarNum(ruedas), Sistema.llenarNum(year),imageFile.getName());
            c = new Camioneta(Sistema.llenarNum(recorrido), Sistema.llenarNum(pasajeros), Sistema.llenarNum(espejos), Sistema.llenarTexto(frenos), Sistema.llenarDatosdelComboBox(transmision), Sistema.llenarTexto(modelo), Sistema.llenarDatosdelComboBox(extraIn));
            v.setDescripcion(c);
            p.setVehiculo(v);
            p.setPersona(persona);
            Files.copy(origen, destino, REPLACE_EXISTING);
            Sistema.agregarVehiculoArchivo(p);
        }else Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Vender vehículo", null, "Ya existe un vehículo con la placa que\ningresó...");
    }
    
    private void venderFurgoneta() throws Exception{
        if(!Sistema.verificarPlaca(placa.getText())){
            p=new Venta(precio.getText());
            v = new Vehiculo(placa.getText(), Sistema.llenarTexto(marca), Sistema.llenarTexto(color),Sistema.llenarDatosdelComboBox(motor), Sistema.llenarNum(ruedas), Sistema.llenarNum(year),imageFile.getName());
            c = new Furgoneta(Sistema.llenarNum(recorrido), Sistema.llenarNum(pasajeros), Sistema.llenarNum(espejos), Sistema.llenarTexto(frenos), Sistema.llenarDatosdelComboBox(transmision), Sistema.llenarTexto(modelo), txExtraIn.getText());
            v.setDescripcion(c);
            p.setVehiculo(v);
            p.setPersona(persona);
            Files.copy(origen, destino, REPLACE_EXISTING);
            Sistema.agregarVehiculoArchivo(p);
        }else Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Vender vehículo", null, "Ya existe un vehículo con la placa que\ningresó...");
    }
    
    private void pedirCarro(){
        titulo.setText("Tipo de descapotable");
        txExtraIn.setVisible(false);
        extraIn.setVisible(true);
        extraIn.setPromptText("Común, caprio, hardtop, targa, etc...");
        ol=new ArrayList<>();
        ol.add("Ninguno");
        ol.add("Común");
        ol.add("Caprio");
        ol.add("Hardtop");
        ol.add("Otro...");
        extraIn.setItems(FXCollections.observableArrayList(ol));
        pasajeros.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[2-5]{0,1}"))? condition: null));
        activarDatos();
    }
    
    private void pedirMoto(){
        titulo.setText("Tipo de timón");
        txExtraIn.setVisible(false);
        extraIn.setVisible(true);
        extraIn.setPromptText("Z-bars, crusier, buckhorn, Otro...");
        ol=new ArrayList<>();
        ol.add("Ninguno");
        ol.add("Z-bars");
        ol.add("Crusier");
        ol.add("Buckhorn");
        ol.add("Otro...");
        extraIn.setItems(FXCollections.observableArrayList(ol));
        activarDatos();
        datoEspecifico(pasajeros, "2");
        datoEspecifico(ruedas,"2");
    }

    private void pedirCamion(){
        titulo.setText("Carga");
        extraIn.setVisible(false);
        txExtraIn.setVisible(true);
        txExtraIn.setPromptText("Capacidad de carga (Puede insertar decimales con punto)");
        pasajeros.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[2-4]{0,1}"))? condition: null));
        activarDatos();
    }
    
    private void pedirCamioneta(){
        titulo.setText("Mecanismo de ventanas");
        txExtraIn.setVisible(false);
        extraIn.setVisible(true);
        extraIn.setPromptText("Eléctrico o Manual");
        ol=new ArrayList<>();
        ol.add("Eléctrico");
        ol.add("Manual");
        extraIn.setItems(FXCollections.observableArrayList(ol));
        pasajeros.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[2-6]{0,1}"))? condition: null));
        activarDatos();
    }
    
    private void pedirBus(){
        titulo.setText("Vidrios");
        extraIn.setVisible(false);
        txExtraIn.setVisible(true);
        txExtraIn.setPromptText("Ingrese el número de vidrios o ventanas");
        pasajeros.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("[1-6]{0,1}[0-9]{0,1}"))? condition: null));
        activarDatos();
    }
    
    private void pedirFurgoneta(){
        titulo.setText("asientos");
        extraIn.setVisible(false);
        txExtraIn.setVisible(true);
        txExtraIn.setPromptText("Número de asientos");
        pasajeros.setTextFormatter(new TextFormatter<>(condition -> (condition.getControlNewText().matches("([2]{0,1}[0]{0,1})?([1]{0,1}[0-8]{0,1})"))? condition: null));
        activarDatos();
    }
    
    private void datoEspecifico(TextField tx,String st){
        tx.setText(st);
        tx.setDisable(true);
    }
    
    private void activarDatos(){
        placa.setDisable(false);
        marca.setDisable(false);
        color.setDisable(false);
        ruedas.setDisable(false);
        year.setDisable(false);
        recorrido.setDisable(false);
        espejos.setDisable(false);
        frenos.setDisable(false);
        motor.setDisable(false);
        transmision.setDisable(false);
        pasajeros.setDisable(false);
        modelo.setDisable(false);
        precio.setDisable(false);
    }
    
    @FXML
    public void adjuntarFoto(MouseEvent event) throws MalformedURLException{
        try{
            imageFile = (new FileChooser()).showOpenDialog(null);
            if(imageFile.getName().endsWith(".jpg")||imageFile.getName().endsWith(".png")||imageFile.getName().endsWith(".jpeg")||imageFile.getName().endsWith(".bmp")||imageFile.getName().endsWith(".psd")||imageFile.getName().endsWith(".tiff")){
                origen = Paths.get(imageFile.getPath());
                destino = Paths.get(System.getProperty("user.dir")+"/src/main/resources/ImVehiculos/"+imageFile.getName());
                Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Cargar una imagen", "Listo!", imageFile.getName()/*"Se ha cargado correctamente la imagen"*/);
                btnRegistrar.setDisable(false);
            }
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Cargar una imagen", "Carga Fallida!", "Algo debió ocurrir con el Sistema,\nPor favor, envía un correo notificando tu problema (Ayuda).");
        }
    }
    
    @FXML
    public void eleccionEM(ActionEvent event){
        if(motor.getValue().toString().equals("Eléctrico")){
            transmision.setValue("Automático");
            transmision.setDisable(true);
        }else if(motor.getValue().toString().equals("Híbrido")){
            transmision.setValue("Automatizada con doble embrague");
            transmision.setDisable(true);
        }
        else{
            transmision.setValue("NO DATA");
            transmision.setDisable(false);
        }
    }
    
    @FXML
    public void regresar(MouseEvent event) throws IOException{
        if(persona.getRol().getTipousuario().size()==2) pasarVC();
        else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Vendedor")) pasarV();
    }
    
    private void pasarVC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedorycomprador");
        Parent form = vc.load();
        VendedorycompradorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosVC(persona);
    }
    
    private void pasarV() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedor");
        Parent form = vc.load();
        VendedorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosV(persona);
    }
    
    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }

    @FXML
    private void guia(ActionEvent event) {
        try {
            Sistema.contactoAyuda(persona);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
