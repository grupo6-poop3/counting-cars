/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;


import ec.edu.espol.model.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.event.*;
import javafx.fxml.*;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
/**
 * FXML Controller class
 *
 * @author Freddy Gilmar
 * @author Andres Medina
 * @author Joaquin Rivadeneira
 */
public class OfertarController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private VBox vPanel;
    @FXML
    private CheckBox precio;
    @FXML
    private CheckBox year;
    @FXML
    private CheckBox recorrido;
    @FXML
    private TextField pInicial;
    @FXML
    private TextField pFinal;
    @FXML
    private TextField aInicial;
    @FXML
    private TextField aFinal;
    @FXML
    private TextField rInicial;
    @FXML
    private TextField rFinal;
    @FXML
    private TextField oferta;
    @FXML
    private TextArea txDatos;
    @FXML
    private ComboBox tipoV;
    @FXML
    private RadioButton orPrecio;
    @FXML
    private RadioButton orYear;
    
    private Persona persona;
    private ArrayList<Venta> listaV,mostrar;
    private ArrayList<Venta> filtroV = new ArrayList<>();
    private ArrayList<Oferta> listaO;
    private Venta ventaActual;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.listaV=Venta.desserializarVenta("Ventas.ser");
        filtroV=listaV;
        txDatos.clear();
        aInicial.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-2]{0,1}[0-9]{0,3}"))? condicion: null ));
        aFinal.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-2]{0,1}[0-9]{0,3}"))? condicion: null ));
        pInicial.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-9]{0,12}"))? condicion: null ));
        pFinal.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-9]{0,12}"))? condicion: null ));
        rInicial.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-9]{0,7}[0-9]{0,7}"))? condicion: null ));
        rFinal.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-9]{0,7}[0-9]{0,7}"))? condicion: null ));
        oferta.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1-9]{0,7}[.]{0,1}[0-9]{0,2}"))? condicion: null ));
        vPanel.getChildren().clear();
        vPanel.setSpacing(15);
        vPanel.setAlignment(Pos.TOP_CENTER);
        mostrandoVehiculos(listaV);
        datosABox();
    }
    
    public void recuperarDatosOf(Persona per){
        this.persona = per;
    }
    
    @FXML
    private void ordenarPrecio(MouseEvent event){
        vPanel.getChildren().clear();
        if(orPrecio.isSelected()){
            orYear.setSelected(false);
            filtroV.sort((v1,v2)->v1.compareTo(v2));
        }
        mostrandoVehiculos(filtroV);
    }
    
    @FXML
    private void ordenarYear(MouseEvent event){
        vPanel.getChildren().clear();
        if(orYear.isSelected()){
            orPrecio.setSelected(false);
            filtroV.sort((v1,v2)->v1.getVehiculo().compareTo(v2.getVehiculo()));
        }
        mostrandoVehiculos(filtroV);
    }
    
    private void datosABox(){
        ArrayList<String> listaTipo = new ArrayList<>();
        listaTipo.add("Carro");
        listaTipo.add("Moto");
        listaTipo.add("Camión");
        listaTipo.add("Camioneta");
        listaTipo.add("Bus");
        listaTipo.add("Furgoneta");
        listaTipo.add("Todos");
        tipoV.setItems(FXCollections.observableArrayList(listaTipo));
    }
    
    @FXML
    public void mostrar(MouseEvent event){
        try{
            if(tipoV.getValue()==null&&!precio.isSelected()&&!year.isSelected()&&!recorrido.isSelected()) throw new InformacionIncompleta("Seleccionar uno de los tipos de vehículos o todos");
            else if(tipoV.getValue()==null) mostrar = Venta.criterioTipoVehiculo(listaV, "Todos");
            else mostrar = Venta.criterioTipoVehiculo(listaV, tipoV.getValue().toString());
            if(precio.isSelected()) mostrar = Venta.criterioPrecio(mostrar,pInicial.getText(), pFinal.getText());
            if(year.isSelected()) mostrar = Venta.criterioAnual(mostrar,aInicial.getText(), aFinal.getText());
            if(recorrido.isSelected()) mostrar = Venta.criterioRecorrido(mostrar,rInicial.getText(), rFinal.getText());
            if(mostrar.isEmpty()) throw new ArchivoNoEncontrado("No hay ventas disponibles con estos parámetros.\nVuelva pronto");
            filtroV=mostrar;
            vPanel.getChildren().clear();
            mostrandoVehiculos(mostrar);
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Mostrar Vehículos", "Parámetros Ingresados", e.getMessage()+"\nRevisar bien los datos ingresados");
        }
    }
    
    private void mostrandoVehiculos(ArrayList<Venta> lista){
        txDatos.clear();
        for(Venta v: lista){
            ImageView iv = estiloImagen(new Image("ImVehiculos/"+v.getVehiculo().getNomFile()));
            iv.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                    ventaActual = v;
                    especificarTipo(v);
                    }
                }
            );
            vPanel.getChildren().add(iv);
        }
    }
    
    private ImageView estiloImagen(Image im){
        ImageView iv = new ImageView(im);
        iv.setFitHeight(100);
        iv.setFitWidth(100);
        iv.setCursor(Cursor.HAND);
        return iv;
    }
    
    private void especificarTipo(Venta v){
        if(v.getVehiculo().getDescripcion() instanceof Carro)
            txDatos.setText("Tipo de vehículo: Carro\n"+v.toString()+((Carro)v.getVehiculo().getDescripcion()).toString());
        else if(v.getVehiculo().getDescripcion() instanceof Moto)
            txDatos.setText("Tipo de vehículo: Moto\n"+v.toString()+((Moto)v.getVehiculo().getDescripcion()).toString());
        else if(v.getVehiculo().getDescripcion() instanceof Camion)
            txDatos.setText("Tipo de vehículo: Camión\n"+v.toString()+((Camion)v.getVehiculo().getDescripcion()).toString());
        else if(v.getVehiculo().getDescripcion() instanceof Camioneta)
            txDatos.setText("Tipo de vehículo: Camioneta\n"+v.toString()+((Camioneta)v.getVehiculo().getDescripcion()).toString());
        else if(v.getVehiculo().getDescripcion() instanceof Bus)
            txDatos.setText("Tipo de vehículo: Bus\n"+v.toString()+((Bus)v.getVehiculo().getDescripcion()).toString());
        else if(v.getVehiculo().getDescripcion() instanceof Furgoneta)
            txDatos.setText("Tipo de vehículo: Furgoneta\n"+v.toString()+((Furgoneta)v.getVehiculo().getDescripcion()).toString());
    }
    
    @FXML
    private void paraPrecio(MouseEvent event){
        if(precio.isSelected()){
            pInicial.setDisable(false);
            pFinal.setDisable(false);
        }else{
            pInicial.setDisable(true);
            pFinal.setDisable(true);
            pInicial.setText("0");
            pFinal.setText("0");
        }
    }
    
    @FXML
    private void paraYear(MouseEvent event){
        if(year.isSelected()){
            aInicial.setDisable(false);
            aFinal.setDisable(false);
        }else{
            aInicial.setDisable(true);
            aFinal.setDisable(true);
            aInicial.setText("0");
            aFinal.setText("0");
        }
    }
    
    @FXML
    private void paraRecorrido(MouseEvent event){
        if(recorrido.isSelected()){
            rInicial.setDisable(false);
            rFinal.setDisable(false);
        }else{
            rInicial.setDisable(true);
            rFinal.setDisable(true);
            rInicial.setText("0");
            rFinal.setText("0");
        }
    }
    
    @FXML
    public void ofertar(MouseEvent event) throws IOException{
       try{
            listaO = ventaActual.getOfertas();
            if(oferta.getText().isBlank()) throw new InformacionIncompleta("Si quiere ingresar una oferta para el vehículo\nasignado, procure llenar el recuadro con el valor ofertado");
            Oferta of = new Oferta(oferta.getText(),persona);
            listaO.add(of);
            ventaActual.setOfertas(listaO);
            Venta.serializarVenta(listaV, "Ventas.ser");
            Oferta.ofertar(of,ventaActual,persona);
            if(persona.getRol().getTipousuario().size()==2) pasarVC();
            else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Comprador")) pasarC();
       }catch(InformacionIncompleta e){
         Sistema.showMessage(new Alert(Alert.AlertType.WARNING),"Ofertar un vehículo",null,e.getMessage());
       }catch(Exception e){Sistema.showMessage(new Alert(Alert.AlertType.WARNING),"Ofertar un vehículo",null,"Asegúrese de selccionar uno de los vehículos\nmostrados en pantalla, ingresar el valor\na ofertar y de ahí presionar el botón para ofertar");}
    }
    
    private void pasarVC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedorycomprador");
        Parent form = vc.load();
        VendedorycompradorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosVC(persona);
    }
    
    private void pasarC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("comprador");
        Parent form = vc.load();
        CompradorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosC(persona);
    }
    
    @FXML
    public void regresar(MouseEvent event) throws IOException{
        if(persona.getRol().getTipousuario().size()==2) pasarVC();
        else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Comprador")) pasarC();
    }

    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }

    @FXML
    private void guia(ActionEvent event) {
        try {
            Sistema.contactoAyuda(persona);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}