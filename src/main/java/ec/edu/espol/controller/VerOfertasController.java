/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class VerOfertasController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private HBox hPanel;
    @FXML
    private VBox vPanel;
    @FXML
    private ComboBox placas;
    
    private String placa;
    private Oferta offer;
    private ArrayList<Oferta> listaOfertas;
    private ArrayList<Venta> listaVentas;
    private ArrayList<String> listaplacas=new ArrayList<>();;
    private Persona persona;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        hPanel.setSpacing(20);
        vPanel.setSpacing(40);
    }
    
    private void añadirListaPlacas(){
        if(!listaVentas.isEmpty()){
            listaplacas.add("Todos");
            placas.setItems(FXCollections.observableArrayList(listaplacas));
        }
        else{
            Sistema.showMessage(new Alert(Alert.AlertType.WARNING), "Vehiculos a la venta", null, "Usted no ha puesto a la venta ningún vehículo...");
            try {
                if(persona.getRol().getTipousuario().size()==2) pasarVC();
                else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Vendedor")) pasarV();
            } catch (IOException ex) {Sistema.showMessage(new Alert(Alert.AlertType.WARNING), "Vehiculos a la venta", null, null);}
        }
    }
    
    public void recuperarDatosLO(Persona per){
        this.persona = per;
        try{
            listaVentas = Venta.desserializarVenta("Ventas.ser");
            for(Venta v : listaVentas){
                if(!v.getOfertas().isEmpty()&&Objects.equals(v.getPersona(), persona)){
                    listaplacas.add(v.getVehiculo().getPlaca());
                    mostrandoVehiculos(v);
                }
            }
            añadirListaPlacas();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void mostrandoVehiculos(Venta v){
        ImageView iv = estiloImagen(new Image("ImVehiculos/"+v.getVehiculo().getNomFile()));
        hPanel.getChildren().add(iv);
        iv.setOnMouseClicked(
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent t){
                    placa = v.getVehiculo().getPlaca();
                    vPanel.getChildren().clear();
                    listaOfertas = v.getOfertas();
                    presentarOfertas();
                }});
    }
    
    private void presentarOfertas(){
        listaOfertas.sort((of1,of2)->of1.compareTo(of2));
        for(Oferta o : listaOfertas){
            Text t = new Text(o.toString());
            t.setCursor(Cursor.HAND);
            t.setOnMouseClicked(
                    new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent t){
                            offer = o;
                        }});
            vPanel.getChildren().add(t);
        }
    }
    
    private ImageView estiloImagen(Image im){
        ImageView iv = new ImageView(im);
        iv.setFitHeight(100);
        iv.setFitWidth(100);
        iv.setCursor(Cursor.HAND);
        return iv;
    }
    
    private void pasarVC() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedorycomprador");
        Parent form = vc.load();
        VendedorycompradorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosVC(persona);
    }
    
    private void pasarV() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("vendedor");
        Parent form = vc.load();
        VendedorController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosV(persona);
    }
    
    @FXML
    private void aceptarOferta(MouseEvent event){
        try{
            if(offer ==null) throw new InformacionIncompleta("Seleccione una de las ofertas para aceptar la que desea");
            Venta.aceptarOferta(placa,persona,offer);
            if(persona.getRol().getTipousuario().size()==2) pasarVC();
            else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Vendedor")) pasarV();
        }catch(InformacionIncompleta e){
            Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Vehículo vendido", null, e.getMessage());
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Vehículo vendido", null, "Ha ocurrido un error al tratar de notificar al comprador su venta");
        }
    }
    
    @FXML
    private void buscar(ActionEvent event){
        try{
            if(placas.getValue()==null) throw new InformacionIncompleta("Escoja una de las opciones");
            hPanel.getChildren().clear();
            vPanel.getChildren().clear();
            for(Venta v: listaVentas){
                if(v.getVehiculo().getPlaca().equals(placas.getValue().toString())) mostrandoVehiculos(v);
                else if(placas.getValue().toString().equals("Todos")&&!v.getOfertas().isEmpty()) mostrandoVehiculos(v);
            }
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.WARNING), "Buscar por placa", null, e.getMessage());
        }
    }
    
    @FXML
    public void regresar(MouseEvent event) throws IOException{
        if(persona.getRol().getTipousuario().size()==2) pasarVC();
        else if(Objects.equals(persona.getRol().getTipousuario().get(0),"Vendedor")) pasarV();
    }
    
    
    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }

    @FXML
    private void guia(ActionEvent event) {
        try {
            Sistema.contactoAyuda(persona);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}