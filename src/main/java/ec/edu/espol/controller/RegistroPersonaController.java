/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 * 
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */

public class RegistroPersonaController implements Initializable {

    
    @FXML
    private TextField stUs;
    @FXML
    private TextField stMail;
    @FXML
    private TextField stNombre;
    @FXML
    private TextField stApel;
    @FXML
    private TextField stCed;
    @FXML
    private TextField stOrg;
    @FXML
    private PasswordField stClav;
    @FXML
    private Text txErrorCor;
    @FXML
    private CheckBox vendedor;
    @FXML
    private CheckBox comprador;
    
    private ArrayList<String> rol;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stCed.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[0][0-9]{0,9}"))? condicion: null ));
        stClav.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches(".{0,15}"))? condicion: null ));
        stUs.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[\\w]{0,15}"))? condicion: null ));
        stNombre.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[a-zA-Z\\s]{0,25}"))? condicion: null ));
        stApel.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[a-zA-Z\\s]{0,25}"))? condicion: null ));
        stOrg.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[a-zA-Z\\s]{0,40}"))? condicion: null ));
    }    
    
    
    
    @FXML
    private void registrar(MouseEvent event){
        try{
            txErrorCor.setText("");
            Rol r = new Rol(stUs, stClav,stMail,rol);
            Persona.guardarPersona(stNombre,stApel,stCed,stOrg,stClav,r);
            App.setRoot("MenuPrincipal");
        }catch(CorreoIncorrecto e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR),"Creación de cuenta",null,"Wow! No se ha podido crear el usuario\n"+e.getMessage());
            txErrorCor.setText("*");
        }catch(Exception e){
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR),"Creación de cuenta",null,"Wow! No se ha podido crear el usuario\n"+e.getMessage());
        }
    }
    
    
    @FXML
    private void agregar(MouseEvent event){
        ArrayList<String> tipo_usuario = new ArrayList<>();
        if(vendedor.isSelected()&&comprador.isSelected()){
            tipo_usuario .add("Vendedor");
            tipo_usuario .add("Comprador");
            rol = tipo_usuario;
        }else if(vendedor.isSelected()&&!comprador.isSelected()){
            tipo_usuario .add("Vendedor");
            rol = tipo_usuario;
        }
        else if(comprador.isSelected()&&!vendedor.isSelected()){
            tipo_usuario .add("Comprador");
            rol = tipo_usuario;
        }
        else rol = tipo_usuario;
    }
    
    @FXML
    private void regresar(MouseEvent event) throws IOException{
        App.setRoot("MenuPrincipal");
    }
    
    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void borrarU(ActionEvent event){
        if((new File("Persona.ser")).exists()) if((new File("Persona.ser")).delete()) Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Archivo de Usuarios", "Borrado satisfactoriamente", "Los datos del archivo Usuarios han sido borrados");
        else Sistema.showMessage(new Alert(Alert.AlertType.WARNING),"Archivo de Usuarios","Error al borrar","No se ha podido borrar el archivo");
    }
    
    @FXML
    private void borrarV(ActionEvent event){
        if((new File("Ventas.ser")).exists()) if((new File("Ventas.ser")).delete()) Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Archivo de Vehiculos", "Borrado satisfactoriamente", "Los datos del archivo Vehiculos han sido borrados");
        else Sistema.showMessage(new Alert(Alert.AlertType.WARNING),"Archivo de Usuarios","Error al borrar","No se ha podido borrar el archivo");
    }
    
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }

}
