/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;
import ec.edu.espol.model.*;
import ec.edu.espol.gui.App;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *@author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class CompradorController implements Initializable {

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */

    private Persona persona;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    public void recuperarDatosC(Persona per){
        this.persona = per;
    }
    
    @FXML
    private void accionRegresar(MouseEvent event) throws IOException{
        App.setRoot("MenuPrincipal");
    }
    
    @FXML
    private void cambiarP(MouseEvent event) throws IOException{
        FXMLLoader vc = Sistema.loadFXML("perfil");
        Parent form = vc.load();
        PerfilController vcc = vc.getController();
        vcc.recuperarDatosP(persona);
        App.setRoot(form);
    }
    
    private void pasarOfertar() throws IOException{
        FXMLLoader vc = Sistema.loadFXML("ofertarV");
        Parent form = vc.load();
        OfertarController vcc = vc.getController();
        App.setRoot(form);
        vcc.recuperarDatosOf(persona);
    }
    
    @FXML
    private void ofertar(MouseEvent event){
        try{
            ArrayList<Venta> listaV= Venta.desserializarVenta("Ventas.ser");
            if(listaV.isEmpty()) throw new ArchivoNoEncontrado("No hay ventas disponibles con estos parámetros.\nVuelva pronto");
            pasarOfertar();
        }catch(ArchivoNoEncontrado e){
            Sistema.showMessage((new Alert(Alert.AlertType.WARNING)), "Ofertar un vehículo", null, e.getMessage());
        }catch(Exception e){
            Sistema.showMessage((new Alert(Alert.AlertType.ERROR)), "Ofertar un vehículo", null, "No se ha podido acceder a esta sección");
        }
    }
    
    @FXML
    private void accionSalir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }
    
    @FXML
    private void guia(ActionEvent event) {
        try {
            Sistema.contactoAyuda(persona);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}