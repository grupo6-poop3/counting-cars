/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Sistema;
import ec.edu.espol.gui.App;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.*;
import java.io.*;
import javafx.event.*;
import javafx.scene.control.*;
import javafx.scene.input.*;

/**
 * FXML Controller class
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Joaquin Rivadeneira
 */
public class MenuPrincipalController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Button regresar; 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    private void iniciarSesion(MouseEvent event) throws IOException{
        App.setRoot("login");
    }
    
    @FXML
    private void registrar(MouseEvent event) throws IOException{
        App.setRoot("registroPersona");
    }
    
    @FXML
    private void accionSalir(MouseEvent event) throws IOException{
        System.exit(0);
    }
    
    @FXML
    private void salir(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void menu(ActionEvent event) throws IOException {
        App.setRoot("MenuPrincipal");
    }
    
    @FXML
    private void borrarU(ActionEvent event){
        Sistema.eliminarArchivo(new Alert(Alert.AlertType.INFORMATION),"Persona.ser", "Archivo de Usuarios", "Los datos del archivo Usuarios han sido borrados","No se ha podido borrar los datos");
    }
    
    @FXML
    private void borrarV(ActionEvent event){
        Sistema.eliminarArchivo(new Alert(Alert.AlertType.INFORMATION),"Ventas.ser", "Archivo de Veh�culos", "Los datos del archivo Vehiculos han sido borrados","No se ha podido borrar los datos");
    }
}